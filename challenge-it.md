# Download 2018 - Hackathon Challenge

## Introduzione

Questo documento è destinato ai partecipanti alla hackathon organizzata durante l'evento [Download 2018](https://download-event.io) e contiene presentazione e informazioni relative alle sfide proposte.

## Indicazioni generali

Facendo riferimento allo spirito dell'evento, ovvero

> 2 giorni di conoscenza, apprendimento, condivisione, progetti, ispirazioni, creatività e **Innovazione**

la risposta alle sfide proposte può essere presentata in diversi modi: idee, proposte di architetture, algoritmi, mockup, prototipi e tutto quanto possa stimolare la fantasia e favorire una partecipazione appassionata degli hacker di tutto il mondo.

Per questo motivo abbiamo scelto di:

- Non imporre restrizioni tecnologiche: usate basi di dati, strumenti, linguaggi, framework e piattaforme che più vi piacciono! (Compatibilmente con la [Licenza di rilascio](https://opensource.org/licenses))
- Limitare i dettagli relativi a requisiti tecnici per eventuali integrazioni con piattaforme esistenti: ove possibile indichiamo nelle sfide la presenza di sistemi esistenti con cui sarebbe utile o interessante dialogare.
- Presentare non una ma molte sfide, di diversa natura. Potete concentrarvi su un particolare aspetto delle problematiche proposte oppure provare ad affrontarle tutte.

----

# Il contesto

La tematica di riferimento è quella dell'invio di **segnalazioni di problemi al Comune** da parte dei cittadini. Possiamo riassumere un generico sistema di segnalazione con i seguenti passaggi:

1. Il cittadino compila i dati relativi alla propria utenza.
2. Il cittadino seleziona la tipologia di segnalazione da un elenco (es.: Illuminazione pubblica, Acquedotto, Segnaletica stradale, etc.).
3. Il cittadino compila una descrizione della segnalazione: descrizione del problema, zona/via, eventuali immagini.
4. L'ufficio di competenza riceve ed elabora la segnalazione.
5. Il cittadino riceve feedback sullo stato della segnalazione: presa in carico, commenti, tempi previsti di risoluzione, conferma risoluzione.

----

# Le sfide

Grazie all'esperienza acquisita, il Comune di Bergamo ci ha aiutato a formulare le seguenti sfide:

## Sfida 1: Eliminazione duplicati

Per garantire una gestione più efficiente è indispensabile ridurre la quantità di segnalazioni duplicate: in caso di malfunzionamento di un lampione probabilmente più cittadini invieranno una richiesta. Come identificare le segnalazioni duplicate? Possiamo mostrare al cittadino alcune proposte *("Did you mean...?")* ancor prima che completi la fase di inserimento della richiesta?

*Segnaliamo la presenza del servizio [Bergamo WiFi](http://wifi.comune.bergamo.it) come una tra le possibili sorgenti di meta-informazioni sulla segnalazione.*

## Sfida 2: Routing verso l'Ufficio competente

Come identificare con la maggiore precisione possibile l'Ufficio di competenza della segnalazione? Quali regole implementereste avendo a disposizione una base dati con lo storico delle segnalazioni e le eventuali correzioni (inoltro ad altri uffici)?
Quanto è affidabile la categorizzazione effettuata dal cittadino? Ci sono categorie più soggette a indirizzamento errato?

*Nota: la base dati non sarà effettivamente fornita durante la competizione.*

## Sfida 3: Feedback al cittadino

Una volta aperta la segnalazione, come comunicare al cittadino la presa in carico della richiesta? Cosa suggerite per mantenere "viva" la comunicazione? In che modo il cittadino può visualizzare lo stato delle richieste inviate? Può essere utile un sistema di votazione delle segnalazioni per mettere in evidenza quelle più gettonate?

*Segnaliamo la possibilità di integrazione con la [piattaforma GIS](https://territorio.comune.bergamo.it/gfmaplet/?map=Orologis&initialExtent=551629;5060087;552645;5060586;32632&htmlstyle=combg) del Comune e il progetto [IO Italia](https://io.italia.it/) ([GitHub](https://github.com/teamdigitale/italia-app)).*

## Sfida 4: Sicurezza

Quali misure adottereste per proteggere il sistema da abusi e attacchi informatici? Esiste un modo per rendere il servizio fruibile da parte del cittadino, eventualmente consentendo segnalazioni anonime ma senza rendere troppo onerosa la procedura di apertura di una segnalazione? Possiamo pensare di realizzare un CAPTCHA migliore di quelli più diffusi o di utilizzare approcci alternativi?

## Sfida 5: Reperibilità delle informazioni

I dati contenuti in un sistema per la gestione delle segnalazioni potrebbero essere di interesse per diversi altri sistemi. Quali tipologie di API implementereste per renderne la consultazione facile, completa e il più flessibile possibile da parte di colleghi sviluppatori?

*Esempio di utilizzo: il servizio di scrivania comunale del cittadino potrebbe essere esteso per prelevare segnalazioni e relativo stato di risoluzione.*

## Sfida 6: Visualizzazione & Gamification

Volendo visualizzare lo stato attuale delle segnalazioni (chiuse, aperte, in processo) in una o più dashboard, quali dati e in che formato suggerite di rappresentarli?
Quali indicatori e funzionalità favorirebbero una sana competizione tra gli Uffici migliorandone l'efficienza?

----

# Note aggiuntive

## La valutazione

Il progetto presentato da ciascun team sarà valutato secondo i criteri di:
* Utilità / Valore per il committente
* Attinenza al tema e alle sfide presentate
* Completezza: verranno premiati i team che hanno saputo portare il progetto in fase più avanzata di elaborazione / realizzazione; sarà considerata anche l'attenzione ai requisiti tecnici funzionali (quali facilità di rilascio, eventuale documentazione prodotta, etc.)
* Design / UX, interfaccia utente, facilità d'uso, appeal
* Creatività / Innovazione soprattutto nell'affrontare le challenge

## Pubblicazione progetti

Verranno valutati i progetti pubblicati entro i termini previsti sul [progetto gitlab ufficiale](https://gitlab.com/download2018/) dell'evento. Durante lo svolgimento dell'hackathon, gli organizzatori procederanno alla creazione di un repository dedicato per ogni team e a garantire gli accessi ai membri (è richiesta la creazione di un account su gitlab.com).
E' consentito utilizzare altre piattaforme per la condivisione del codice ed effettuare un push finale sul repository del proprio team.

----

Buon divertimento,  
*Gli owner dell'Hackathon*

